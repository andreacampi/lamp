name "cluster-web"
description "System is added to the cluster as an nginx web front end."
run_list  "role[bootstrap]",
          "role[lamp-cluster-config]",
          "recipe[lamp-cluster::web]"
