name "bootstrap"
description "Bootstrap a node run"
run_list "recipe[ohai]", "recipe[vagrant-ohai]"
default_attributes({
  :ohai => {
    :plugins => {
      'vagrant-ohai' => 'plugins'
    }
  }
})
