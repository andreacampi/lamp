name "cluster-app"
description "System is added to the LAMP cluster as an unicorn rails app server"
run_list  "role[bootstrap]",
          "role[lamp-cluster-config]",
          "recipe[lamp-cluster]"
