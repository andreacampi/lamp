name "web"
description "System is ready to run the nginx web server"
run_list  "role[bootstrap]",
          "recipe[lamp-node-web]"
