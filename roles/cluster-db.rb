name "cluster-db"
description "System is added to the LAMP cluster as a MySQL db server"
run_list  "role[bootstrap]",
          "role[lamp-cluster-config]",
          "recipe[lamp-cluster]"
