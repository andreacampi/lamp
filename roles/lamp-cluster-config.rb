name "lamp-cluster-config"
description "Configuration for a LAMP cluster"

default_attributes "lamp-cluster-config" => {
  "cluster_id" => "lamp-cluster-1",
  "app" => {
    "nodes" => ["10.10.10.19", "10.10.10.20"]
  },
  "db" => {
    "nodes" => ["10.10.10.17", "10.10.10.18"],
    "master" => "cluster-db-0"
  }
}
