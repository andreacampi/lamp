require 'chef-workflow/helper'

class ClusterTest < MiniTest::Unit::VagrantTestCase

  def setup
    provision('cluster-db', 2)
    # provision('cluster-app', 1, ['cluster-db'])
    # provision('cluster-web', 1, ['cluster-app'])
  end

  def teardown
    # deprovision('cluster-app')
    # deprovision('cluster-db')
    # deprovision('cluster-web')
  end

  # def test_we_have_one_app_server
  #   wait_for('cluster-app')
  #   assert_search_count(:node, 'roles:cluster-app', 1)
  # end

  # def test_we_have_one_web_server
  #   wait_for('cluster-web')
  #   assert_search_count(:node, 'roles:cluster-web', 1)
  # end

  def test_we_have_two_db_servers
    wait_for('cluster-db')
    assert_search_count(:node, 'roles:cluster-db', 2)
  end
end
